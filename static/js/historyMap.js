/*
  Raphaël 2.1.0 - JavaScript Vector Library                         
  Copyright © 2008-2012 Dmitry Baranovskiy (http://raphaeljs.com)   
  Copyright © 2008-2012 Sencha Labs (http://sencha.com)             
  Licensed under the MIT (http://raphaeljs.com/license.html) license. 
  ***
  Document   : Vectoral Mongolian Map with RaphaelJS
  @author Margulan BYEKMURAT <http://margulan.com> <http://twitter.com/margulan @margulan>
  @editor   @author Damdinsuren ENKHBAYAR <http://enkhbayar.blog.gogo.mn> <http://twitter.com/enkheee>
*/
function historyMapStart()
{
			var tekser="";
			var tekser1="";
            var R = Raphael("paper", 1000, 675);
            var attr = {
                fill: "#e0e0e0",
                stroke: "#0197b8",
                "stroke-width": 0.15,
                "stroke-linejoin": "round",
				"transform": " s3.5,3.5,0,0" 
            };
            var aus = {};
            var current = null;
			var i=0;
			for (var state in paths)
			{
				i++;
            	aus[i] = R.path(paths[state].path).attr(attr);
				aus[i].name = paths[state].name;
				aus[i].about = paths[state].about;
			}
            for (var state in aus) {
                aus[state].color = Raphael.getColor();
                (function (st, state) {
                    st[0].style.cursor = "pointer";
                    st[0].onmouseover = function () {
                        current && aus[current].animate({fill: "#e0e0e0", stroke: "#0197b8"}, 300);
                        st.animate({fill: "#8297ac", stroke: "#0197b8"}, 300);
                        st.toFront();
                        R.safari();
						$("#sehirTakirp h3").text(capitaliseFirstLetter(aus[state].name));
						$("#mapTitle").text(capitaliseFirstLetter(aus[state].name));
                        current = state;
                    };
                    st[0].onmouseout = function () {
                        st.animate({fill: "#e0e0e0", stroke: "#0197b8"}, 300);
                        st.toFront();
                        R.safari();
                    };
					st[0].onmouseup = function ()
					{
						if (tekser==aus[state].name)
						{
							$('#main_messsage_box').modal({
							  dynamic:true
							});							
							$("#sehirTakirp h1").text(capitaliseFirstLetter(aus[state].name));
						    $("#mapTitle").text("");
						}
						if (tekser1==aus[state].about)
						{
							$('#main_messsage_box').modal({
							 dynamic:true
							});					
							$("#main_messsage_box_into").html(capitaliseFirstLetter(aus[state].about));
						}
					};
					
					st[0].onmousedown = function ()
					{
						tekser=aus[state].name;
						tekser1=aus[state].about;
					};
                    if (state == "nsw") {
                        st[0].onmouseover();
                    }
                })(aus[state], state);
            }
}
function capitaliseFirstLetter(string)
{
	return string.charAt(0).toUpperCase() + string.slice(1);
}
