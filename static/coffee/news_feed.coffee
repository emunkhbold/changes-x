class UserPost
  init: () ->
    post_form = $('form#post')
    post_form.submit @add_post
    $(document).on('submit', 'form.comment', userPost.add_comment)
    UserPost.url = @url
    $(document).on('click', '.write_comment', ()->
      $(@).prev().toggle()
    )
    $(document).on('ready', 'object', ()->
      $(@).removeAttr('autoplay')
    )
    $(document).on('click', '.view', ()->
      $(@).next().hide()
    )

  
  add_post: () ->
    data = new FormData($(@)[0])
    userPost.ajax postUrl, data, 'json', userPost.post_success
    false

  add_comment: () ->
    $(@)[0].post.value = $(@)[0].id
    data = new FormData($(@)[0])
    userPost.ajax commentUrl, data, 'json', userPost.comment_success
    false

  comment_success: (data) ->
    object = $.parseJSON(data['comment'])
    comment = object[0].fields
    container = $("form##{comment.post}").prev('div').children()
    file = object[1].fields if object[1]
    html = "<li class='list-comments' >"
    if file?
      html += "<span><img src='static/#{file.shared_file}'style='width:300px;height:220px' /></span>"
      html += "<a href='#{file.shared_file}'>Үзэх</a>"
    if comment?
      html += "<p class='list-group-item-text'>#{comment.text}</p>"
    html += "</li>"
    container.append(html)

  post_success: (data) ->
    container = $('div#posts')
    object = $.parseJSON(data['post'])
    post = object[0].fields
    file = object[1].fields if object[1]
    html = "<div class='list-group-item'>"
    if file?
      html += "<span><object src='static/#{file.shared_file}' width='320px' height='256px'></object</span><a href='download/{{ share.shared_file.name }}'>Татах</a>"
      html += "<a href='#{file.shared_file}'>Үзэх</a>"
    if post?
      html += "<h5>#{post.text}</h5>"
    html += "<div><ul></ul></div></div>"
    container.prepend(html)
    up = userPost
    post = object[0]
    post_id = object[0].pk
    data = JSON.stringify('post_id': post_id)
    up.ajax('load_form', data, 'json', up.load_comment_form,
      up.set_csrf_token, 'text json': true)
    false

  load_comment_form: (data) ->
    container = $('div#posts div.list-group-item').first().append(data)
    container.append("<a href='#' class='write_comment'> write comment ...</a>")

  set_csrf_token: (xhr) ->
    xhr.setRequestHeader("X-CSRFToken", userPost.getCookie('csrftoken'))

  ajax: (url, data, data_type, success, before_send, converters) ->
    $.ajax
      type: 'POST'
      url: url
      data: data
      beforeSend: before_send
      success: success
      processData: false
      contentType: false
      dataType: data_type
      converters: converters

  getCookie: (name) ->
    cookieValue = null
    if document.cookie?
      cookies = document.cookie.split(';')

      for cookie in cookies
        cookie = $.trim(cookie)
        if cookie.substring(0, name.length + 1) == (name + '=')
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
          break

    cookieValue


@get_user_post = () ->
  new UserPost()
