-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2013 at 08:27 PM
-- Server version: 5.5.31
-- PHP Version: 5.4.4-14+deb7u3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `car`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkUser`(IN name varchar(30), IN uId INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
select * from auth_user where name=username and uId=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `countCar`(IN empID INT, OUT  count_car INT)
select count(*) into count_car from controller_car where employer_id=empID$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add permission', 1, 'add_permission'),
(2, 'Can change permission', 1, 'change_permission'),
(3, 'Can delete permission', 1, 'delete_permission'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add user', 3, 'add_user'),
(8, 'Can change user', 3, 'change_user'),
(9, 'Can delete user', 3, 'delete_user'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add session', 5, 'add_session'),
(14, 'Can change session', 5, 'change_session'),
(15, 'Can delete session', 5, 'delete_session'),
(16, 'Can add site', 6, 'add_site'),
(17, 'Can change site', 6, 'change_site'),
(18, 'Can delete site', 6, 'delete_site'),
(19, 'Can add log entry', 7, 'add_logentry'),
(20, 'Can change log entry', 7, 'change_logentry'),
(21, 'Can delete log entry', 7, 'delete_logentry'),
(22, 'Can add migration history', 8, 'add_migrationhistory'),
(23, 'Can change migration history', 8, 'change_migrationhistory'),
(24, 'Can delete migration history', 8, 'delete_migrationhistory'),
(25, 'Can add client', 9, 'add_client'),
(26, 'Can change client', 9, 'change_client'),
(27, 'Can delete client', 9, 'delete_client'),
(28, 'Can add employers', 10, 'add_employers'),
(29, 'Can change employers', 10, 'change_employers'),
(30, 'Can delete employers', 10, 'delete_employers'),
(31, 'Can add car', 11, 'add_car'),
(32, 'Can change car', 11, 'change_car'),
(33, 'Can delete car', 11, 'delete_car'),
(34, 'Can add reservation', 12, 'add_reservation'),
(35, 'Can change reservation', 12, 'change_reservation'),
(36, 'Can delete reservation', 12, 'delete_reservation'),
(37, 'Can add pay', 13, 'add_pay'),
(38, 'Can change pay', 13, 'change_pay'),
(39, 'Can delete pay', 13, 'delete_pay');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  `INSERT` tinyint(1) NOT NULL,
  `UPDATE` tinyint(1) NOT NULL,
  `DELETE` tinyint(1) NOT NULL,
  `DROP` tinyint(1) NOT NULL,
  `CREATE` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`, `INSERT`, `UPDATE`, `DELETE`, `DROP`, `CREATE`) VALUES
(1, 'pbkdf2_sha256$10000$edqjJqwu5kRA$rKXj82wXhNhJN5eAF6rZIj+cq9Im1T6seJWq0nNI2YI=', '2013-11-22 23:46:03', 1, 'sublime', '', '', 'apple.muugii@gmail.com', 1, 1, '2013-10-10 10:51:43', 0, 0, 0, 0, 0),
(2, 'pbkdf2_sha256$10000$MRZLSkWb23z1$/hpGxpXXpPZF1Xd6u+1RLgb53wj5Lyh13I7J9t8enBg=', '2013-11-22 23:44:14', 0, 'testUser', 'Test ', 'User', 'test1@gmail.com', 0, 1, '2013-11-22 23:44:14', 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `controller_car`
--

CREATE TABLE IF NOT EXISTS `controller_car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `onOff` tinyint(1) NOT NULL,
  `employer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_car_0f030170` (`employer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `controller_car`
--

INSERT INTO `controller_car` (`id`, `number`, `onOff`, `employer_id`) VALUES
(1, '1234', 1, 1),
(3, '4659', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `controller_client`
--

CREATE TABLE IF NOT EXISTS `controller_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `registered_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_client_d4e7917a` (`number`),
  KEY `controller_client_f5836880` (`lname`),
  KEY `controller_client_9cbcd7a1` (`fname`),
  KEY `controller_client_3ac8a70a` (`address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `controller_client`
--

INSERT INTO `controller_client` (`id`, `number`, `lname`, `fname`, `address`, `registered_at`) VALUES
(1, '99376603', 'Наранбаяр', 'Ууганбаяр', 'Дархан', '2013-10-15 10:20:14'),
(2, '99231640', 'Бямбадорж', 'Пунцаг', 'Хөвсгөл', '2013-10-15 10:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `controller_employers`
--

CREATE TABLE IF NOT EXISTS `controller_employers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `registered_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_employers_f5836880` (`lname`),
  KEY `controller_employers_9cbcd7a1` (`fname`),
  KEY `controller_employers_3ac8a70a` (`address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `controller_employers`
--

INSERT INTO `controller_employers` (`id`, `lname`, `fname`, `address`, `salary`, `position`, `registered_at`) VALUES
(1, 'Бат-Орших', 'Нямбаяр', 'модны 2', 3000000, 0, '2013-10-15 10:21:05'),
(2, 'Мөнхболд', 'Энхтөр', '1-р хороолол', 1000000, 1, '2013-10-15 10:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `controller_pay`
--

CREATE TABLE IF NOT EXISTS `controller_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mission` tinyint(1) NOT NULL,
  `take` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_pay_c34c40a3` (`take`),
  KEY `controller_pay_3032bfd2` (`car_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `controller_pay`
--

INSERT INTO `controller_pay` (`id`, `mission`, `take`, `car_id`, `time`) VALUES
(1, 1, 10000, 1, '2013-10-16 00:59:05'),
(2, 0, 5000, 3, '2013-10-16 00:59:18');

-- --------------------------------------------------------

--
-- Table structure for table `controller_reservation`
--

CREATE TABLE IF NOT EXISTS `controller_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `controller_reservation_4fea5d6a` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `user_id`, `content_type_id`, `object_id`, `object_repr`, `action_flag`, `change_message`) VALUES
(1, '2013-10-15 05:11:13', 1, NULL, NULL, 'hehe', 1, 'Change_message'),
(2, '2013-10-15 00:00:00', 1, 1, NULL, 'haha', 4, 'haha'),
(3, '2013-10-15 10:20:14', 1, 9, '1', 'Наранбаяр', 1, ''),
(4, '2013-10-15 10:20:32', 1, 9, '2', 'Бямбадорж', 1, ''),
(5, '2013-10-15 10:21:05', 1, 10, '1', 'Бат-Орших', 1, ''),
(6, '2013-10-15 10:21:23', 1, 10, '2', 'Мөнхболд', 1, ''),
(7, '2013-10-15 10:21:53', 1, 11, '1', '1234', 1, ''),
(8, '2013-10-15 10:22:25', 1, 11, '2', '4651', 1, ''),
(9, '2013-10-15 11:08:49', 1, 11, '3', '4659', 1, ''),
(10, '2013-10-15 11:38:23', 1, 11, '2', '4651', 3, ''),
(11, '2013-10-15 11:59:07', 1, 13, '1', '1234', 1, ''),
(12, '2013-10-15 11:59:23', 1, 13, '2', '4659', 1, ''),
(15, '2013-10-15 00:00:00', 1, 1, 'asdfasd', 'asdfa', 0, 'sfhakfahk'),
(16, '2013-11-22 23:44:14', 1, 3, '2', 'testUser', 1, ''),
(17, '2013-11-22 23:44:50', 1, 3, '2', 'testUser', 2, 'Changed password, first_name, last_name and email.');

--
-- Triggers `django_admin_log`
--
DROP TRIGGER IF EXISTS `if_null`;
DELIMITER //
CREATE TRIGGER `if_null` BEFORE INSERT ON `django_admin_log`
 FOR EACH ROW begin
if isnull(new.content_type_id)
then set new.content_type_id = 1;
end if;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'permission', 'auth', 'permission'),
(2, 'group', 'auth', 'group'),
(3, 'user', 'auth', 'user'),
(4, 'content type', 'contenttypes', 'contenttype'),
(5, 'session', 'sessions', 'session'),
(6, 'site', 'sites', 'site'),
(7, 'log entry', 'admin', 'logentry'),
(8, 'migration history', 'south', 'migrationhistory'),
(9, 'client', 'controller', 'client'),
(10, 'employers', 'controller', 'employers'),
(11, 'car', 'controller', 'car'),
(12, 'reservation', 'controller', 'reservation'),
(13, 'pay', 'controller', 'pay');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('8bu8xmt1289qotwggnjyvawc0wygn045', 'NWZkNjBiOGNlODI2ZjFlZmJmNGYxY2RkZDNmM2EwOGM1ZmIyYWI0MzqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==', '2013-12-06 23:46:03'),
('g86m6usim9dh7jxhoxvsraegbqw5dbv8', 'NWZkNjBiOGNlODI2ZjFlZmJmNGYxY2RkZDNmM2EwOGM1ZmIyYWI0MzqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==', '2013-11-29 17:14:05'),
('lzduxm958y2cv12w7zl6dalo3w7ax9w7', 'NWZkNjBiOGNlODI2ZjFlZmJmNGYxY2RkZDNmM2EwOGM1ZmIyYWI0MzqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==', '2013-10-24 21:06:00'),
('u19qar8wqi57sumjobs8wmd4grsimb8s', 'NWZkNjBiOGNlODI2ZjFlZmJmNGYxY2RkZDNmM2EwOGM1ZmIyYWI0MzqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==', '2013-11-09 15:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE IF NOT EXISTS `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `south_migrationhistory`
--

CREATE TABLE IF NOT EXISTS `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `south_migrationhistory`
--

INSERT INTO `south_migrationhistory` (`id`, `app_name`, `migration`, `applied`) VALUES
(1, 'controller', '0001_initial', '2013-10-10 10:52:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `controller_car`
--
ALTER TABLE `controller_car`
  ADD CONSTRAINT `employer_id_refs_id_aa872b3e` FOREIGN KEY (`employer_id`) REFERENCES `controller_employers` (`id`);

--
-- Constraints for table `controller_pay`
--
ALTER TABLE `controller_pay`
  ADD CONSTRAINT `car_id_refs_id_bbc06771` FOREIGN KEY (`car_id`) REFERENCES `controller_car` (`id`);

--
-- Constraints for table `controller_reservation`
--
ALTER TABLE `controller_reservation`
  ADD CONSTRAINT `client_id_refs_id_cfd94993` FOREIGN KEY (`client_id`) REFERENCES `controller_client` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
