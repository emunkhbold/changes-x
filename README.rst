taxi reseravation
=================
#. Runserver::
    python manage.py runserver

 #. Update database::
    python manage.py syncdb
    python manage.py  migrate <app_name>

South commands
==============
-For first migration::
   python manage.py schemamigration <app_name> --initial

- Other time::
   python manage.py schemamigration <app_name> --auto
- To apply this migration::
    python manage.py migrate <app_name>

Coffee Scripts
==============

-To rebuild/watch js files::
    python manage.py jbuild
    python manage.py jswatch

Sass/Scss
=========
- To watch::
    compass watch


https://github.com/jcobb/basic-jquery-slider/
