from django.db import models
from django.contrib.auth.models import User
import datetime


insert = models.BooleanField(default=True)
insert.contribute_to_class(User, 'INSERT')
update = models.BooleanField(default=True)
update.contribute_to_class(User, 'UPDATE')
delete = models.BooleanField(default=True)
delete.contribute_to_class(User, 'DELETE')
drop  = models.BooleanField(default=True)
drop.contribute_to_class(User, 'DROP')
create = models.BooleanField(default=True)
create.contribute_to_class(User, 'CREATE')


class employers(models.Model):
    lname = models.CharField(max_length=20, db_index=True)
    fname = models.CharField(max_length=20, db_index=True)
    address = models.CharField(max_length=20, db_index=True)
    salary = models.IntegerField()
    TYPE_CHOICES = (
        (0, ' Motor Driver '),
        (1, ' Admin '),
    )
    position = models.PositiveIntegerField(
        choices=TYPE_CHOICES,
        null=False
    )
    registered_at = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return unicode(self.lname)


class car(models.Model):
    number = models.CharField(max_length=6)
    onOff = models.BooleanField(default=False)
    employer = models.ForeignKey(employers)
    
    def __unicode__(self):
        return unicode(self.number)

class District(models.Model):
    name = models.CharField(max_length=20)
    
    def __unicode__(self):
        return unicode(self.name)


class reservation(models.Model):
    name = models.CharField(max_length=40,null=False)
    mobile = models.IntegerField(null=False)
    home_number = models.IntegerField(null=False)
    district = models.ForeignKey(District)
    command = models.PositiveIntegerField(null=False)
    position = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return unicode(self.name)


class pay(models.Model):
    mission = models.BooleanField(default=True)
    take = models.IntegerField(db_index=True)
    car = models.ForeignKey(car)
    time = models.DateTimeField()
    
    def __unicode__(self):
        return unicode(self.car)

class News(models.Model):
    header = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return unicode(self.header)

class Feedback(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return "feedback "+unicode(self.id)
    
    def save(self, *args, **kwargs):
        import logging
        logging.error(args)    
        super(Feedback, self).save(*args, **kwargs)


class back(models.Model):
    cont = models.TextField()
    name = models.CharField(max_length=255)


class front(models.Model):
    cont = models.TextField()

#class Million(models.Model):
#    name = models.TextField()
#    date = models.DateTimeField(auto_now_add=True)
#    dec = models.IntegerField()
#    flt = models.FloatField()
#
    

# Create your models here.

