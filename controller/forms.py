# coding: utf-8
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.flatpages.models import FlatPage
from tinymce.widgets import TinyMCE
from controller.models import reservation
import datetime
en_mn = {
    'username': 'Нэвтрэх нэр',
    'first_name': 'Нэр',
    'last_name': 'Овог',
    'password': 'Нууц үг',
    'password1': 'Нууц үг',
    'password2': 'Нууц үг',
    'email': 'Email хаяг',
}

class UserForm(UserCreationForm):
    class Meta:
        attrs = {'class': 'form-control'}
        model = User
        fields = ('first_name', 'last_name', 'username',
                  'email', 'password1', 'password2')

        widgets = {
                'email': forms.TextInput(attrs=attrs),
                'first_name': forms.TextInput(attrs=attrs),
                'last_name': forms.TextInput(attrs=attrs),
        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        for field in ['username', 'password1', 'password2']:
            self.fields[field].widget.attrs = {'class': 'form-control'}

        for field in ['email', 'first_name', 'last_name']:
            self.fields[field].required = True

        for field in UserForm._meta.fields:
            self.fields[field].label = en_mn[field]



class SuperUserForm(forms.Form):
    CREATE  = forms.BooleanField(required=False)
    DROP = forms.BooleanField(required=False)
    INSERT  = forms.BooleanField(required=False)
    UPDATE  = forms.BooleanField(required=False)
    DELETE  = forms.BooleanField(required=False)
    
    def clean(self):
        cd = super(SuperUserForm, self).clean()
        return cd
    
    def save(self, user):
        cd = self.cleaned_data
        for key, data in cd.items():
            if data:
                if key == 'INSERT':
                    user.INSERT = True
                if key == 'DELETE':
                    user.DELETE = True
                if key == 'UPDATE':
                    user.UPDATE = True
                if key == 'DROP':
                    user.DROP = True
                if key == 'CREATE':
                    user.CREATE = True
            else:
                if key == 'INSERT':
                    user.INSERT = False
                if key == 'DELETE':
                    user.DELETE = False
                if key == 'UPDATE':
                    user.UPDATE = False
                if key == 'DROP':
                    user.DROP = False
                if key == 'CREATE':
                    user.CREATE = False
        user.save() 

class LoginForm(AuthenticationForm):
    error_messages = {
        'invalid_login': 'Таны Нэр эсвэл Нууц үг буруу байна.',
        'inactive': 'Таны нэвтрэх эрх цуцлагдсан байна.'
    }

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        for field in ['username', 'password']:
            self.fields[field].widget.attrs = {'class': 'form-control'}

        for field in ['username', 'password']:
            self.fields[field].label = en_mn[field]


class FlatPageForm(forms.ModelForm):
    class Meta:
        model = FlatPage


class ReservationForm(forms.Form):
    position = forms.CharField()
    created_at = forms.DateTimeField(initial=datetime.datetime.now)
    
    def save(self):
        Reservation = reservation()
        Reservation.position = self.cleaned_data.get('position', None)
        #Reservation.client = self.cleaned_data.get('client_id', None) 
        #Reservation.created_at = self.cleaned_data.get('created_at', None)
        Reservation.save()
        return reservation

# class LoginForm(forms.Form):
#     username = forms.CharField(required=True,
#             widget=forms.TextInput(attrs={'class': 'form-control'}))
#     password = forms.CharField(
#             widget=forms.PasswordInput(attrs={'class': 'form-control'}))
