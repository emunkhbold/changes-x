# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'front'
        db.create_table(u'controller_front', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cont', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'controller', ['front'])

        # Adding model 'back'
        db.create_table(u'controller_back', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cont', self.gf('django.db.models.fields.TextField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'controller', ['back'])


    def backwards(self, orm):
        # Deleting model 'front'
        db.delete_table(u'controller_front')

        # Deleting model 'back'
        db.delete_table(u'controller_back')


    models = {
        u'controller.back': {
            'Meta': {'object_name': 'back'},
            'cont': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'controller.car': {
            'Meta': {'object_name': 'car'},
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['controller.employers']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'controller.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'controller.employers': {
            'Meta': {'object_name': 'employers'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'fname': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lname': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'registered_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'salary': ('django.db.models.fields.IntegerField', [], {})
        },
        u'controller.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'controller.front': {
            'Meta': {'object_name': 'front'},
            'cont': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'controller.news': {
            'Meta': {'object_name': 'News'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'header': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'controller.pay': {
            'Meta': {'object_name': 'pay'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['controller.car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mission': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'take': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'controller.reservation': {
            'Meta': {'object_name': 'reservation'},
            'command': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['controller.District']"}),
            'home_number': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['controller']