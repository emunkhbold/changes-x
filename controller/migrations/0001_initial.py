# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'employers'
        db.create_table(u'controller_employers', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lname', self.gf('django.db.models.fields.CharField')(max_length=20, db_index=True)),
            ('fname', self.gf('django.db.models.fields.CharField')(max_length=20, db_index=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=20, db_index=True)),
            ('salary', self.gf('django.db.models.fields.IntegerField')()),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('registered_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'controller', ['employers'])

        # Adding model 'car'
        db.create_table(u'controller_car', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('onOff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('employer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['controller.employers'])),
        ))
        db.send_create_signal(u'controller', ['car'])

        # Adding model 'District'
        db.create_table(u'controller_district', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'controller', ['District'])

        # Adding model 'reservation'
        db.create_table(u'controller_reservation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('mobile', self.gf('django.db.models.fields.IntegerField')()),
            ('home_number', self.gf('django.db.models.fields.IntegerField')()),
            ('district', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['controller.District'])),
            ('command', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'controller', ['reservation'])

        # Adding model 'pay'
        db.create_table(u'controller_pay', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mission', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('take', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['controller.car'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'controller', ['pay'])

        # Adding model 'News'
        db.create_table(u'controller_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('header', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'controller', ['News'])

        # Adding model 'Feedback'
        db.create_table(u'controller_feedback', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'controller', ['Feedback'])


    def backwards(self, orm):
        # Deleting model 'employers'
        db.delete_table(u'controller_employers')

        # Deleting model 'car'
        db.delete_table(u'controller_car')

        # Deleting model 'District'
        db.delete_table(u'controller_district')

        # Deleting model 'reservation'
        db.delete_table(u'controller_reservation')

        # Deleting model 'pay'
        db.delete_table(u'controller_pay')

        # Deleting model 'News'
        db.delete_table(u'controller_news')

        # Deleting model 'Feedback'
        db.delete_table(u'controller_feedback')


    models = {
        u'controller.car': {
            'Meta': {'object_name': 'car'},
            'employer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['controller.employers']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'controller.district': {
            'Meta': {'object_name': 'District'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'controller.employers': {
            'Meta': {'object_name': 'employers'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'fname': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lname': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'registered_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'salary': ('django.db.models.fields.IntegerField', [], {})
        },
        u'controller.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'controller.news': {
            'Meta': {'object_name': 'News'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'header': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'controller.pay': {
            'Meta': {'object_name': 'pay'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['controller.car']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mission': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'take': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'controller.reservation': {
            'Meta': {'object_name': 'reservation'},
            'command': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'district': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['controller.District']"}),
            'home_number': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['controller']