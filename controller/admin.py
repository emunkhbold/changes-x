from django.contrib import admin
from controller.models import employers, car, reservation, pay, News, Feedback
from django.core.urlresolvers import reverse
from django.contrib.flatpages.admin import FlatPageAdmin
from tinymce.widgets import TinyMCE


class TinyMCEFlatPageAdmin(FlatPageAdmin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'Fname':
            return db_field.formfield(widget=TinyMCE(
                attrs={'cols': 80, 'rows': 30},
                mce_attrs={'external_link_list_url': reverse('tinymce.views.flatpages_link_list')},
            ))
        return super(TinyMCEFlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)


class employersAdmin(admin.ModelAdmin):
    search_fields = ['lname', 'fname', 'position', 'salary']
    date_hierarchy = 'registered_at'
    list_filter = ['position', 'registered_at']
    ordering = ['lname']
    list_display = ('lname', 'fname', 'position', 'salary', 'address')


class carAdmin(admin.ModelAdmin):
    list_display = ('employer', 'onOff')
    list_filter = ['onOff']
    search_field = ['employer']
    actions = ['call_proc']


class payAdmin(admin.ModelAdmin):
    list_display = ('car', 'mission', 'take', "time")
    list_filter = ['mission', 'time']
    search_fields = ['car', 'take']
    date_hierarchy = 'time'


class permissionAdmin(admin.ModelAdmin):
    list_display = ('codename', 'name')


class NewsAdmin(admin.ModelAdmin):
    fields = ['header', 'text']
    list_display = ('header', 'created_at')


class ReservationAdmin(admin.ModelAdmin):
    fields = ['name', 'mobile', 'home_number', 'district', 'command',
            'position']
    list_display = ('name', 'position', 'created_at')
    list_filter = ['district', 'created_at']


class FeedBackAdmin(admin.ModelAdmin):
    list_display = ('text', 'created_at')
    fields = ['text']
    list_filter = ['created_at']

admin.site.register(Feedback, FeedBackAdmin)
admin.site.register(reservation, ReservationAdmin)
admin.site.register(News, NewsAdmin)    
admin.site.register(employers, employersAdmin)
admin.site.register(car, carAdmin)
#admin.site.register(FlatPage, TinyMCEFlatPageAdmin)
admin.site.register(pay, payAdmin)
