from django.conf.urls import patterns, url
from controller import views
from controller.forms import LoginForm
from django.contrib.auth.decorators import login_required
urlpatterns = patterns('',
    url(r'^$', login_required(views.PermisionView.as_view()), name='permission'),
    url(r'login', 'django.contrib.auth.views.login', {
                                'template_name': 'controller/logon.html',
                                'authentication_form': LoginForm,}, name='login'),
    url(r'logout', 'django.contrib.auth.views.logout', {'next_page': 'login'}, name='logout'),
    url(r'register', views.Register.as_view(template_name='controller/registration.html')),
    url(r'news$', views.NewsView.as_view(),name='news'),
    url(r'feedback$', views.FeedBack.as_view(), name='feedback'),
    url(r'map$', views.historyMap, name='mongolia'),
    url(r'back/(?P<pk>\d+)$', views.BackView.as_view(), name='back'),
    url(r'front$', views.frnt, name='front'),
    url(r'reserve',
        login_required(views.Res.as_view(template_name='controller/reservation.html')),name='reservation'),

)
