# Create your views here.
from django.views import generic
from controller.forms import SuperUserForm, ReservationForm 
from django.shortcuts import redirect, render
from controller.models import  News
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView
from forms import UserForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.mail import mail_admins
from controller.models import reservation, Feedback, back, front


class NewsView(generic.ListView):
    template_name = 'controller/news.html'
    def get_queryset(self):
        return News.objects.all()
    def get_context_data(self, **kwargs):
        context = super(NewsView, self).get_context_data(**kwargs)
        context.update ({
            'news': News.objects.all(),
        })
        return context


class PermisionView(generic.ListView):
    template_name = 'controller/index.html'
    def get_queryset(self):
        return Feedback.objects.all()
    def get_context_data(self, **kwargs):
        context = super(PermisionView, self).get_context_data(**kwargs)
        context.update ({
            'form': SuperUserForm(),
        })
        return context

@login_required
def change_permission(request):
    form = SuperUserForm(request.POST)
    if form.is_valid():
        form.save(request.user)
    return redirect("controller:permission")

class Register(CreateView):
    form_class = UserForm
    model = User
    success_url = reverse_lazy('controller:login')
    

class Res(CreateView):
    model = reservation
    template_name = 'controller/reservation.html'
    success_url = reverse_lazy("controller:permission")


class FeedBack(CreateView):
    model = Feedback
    template_name = 'controller/feedback.html'
    success_url = reverse_lazy('controller:permission')
    
    #def form_valid(self,form):
    #    mail_admins('easy taxi', unicode('check feedback'), fail_silently=True)
    #    form.save()
    #    return super(FeedBack, self).form_valid(form) 

class ReservationView(generic.ListView):
    template_name = 'controller/reservation.html'
    
    def get_queryset(self):
        return Feedback.objects.all()
    
    def get_context_data(self, **kwargs):
        context = super(ReservationView, self).get_context_data(**kwargs)
        context.update({
            'form': ReservationForm()
        })
        return context

def historyMap(request):
    return render(request, "controller/map.html")



class BackView(generic.DetailView):
    model = back
    template_name = 'controller/back.html'
    
    def get_context_data(self, **kwargs):
        context = super(BackView, self).get_context_data(**kwargs)
        context.update({
            'cont': back.objects.filter(pk=self.object.id)
        })
        return context
   
    def post(self, request, *args, **kwargs):
        front.objects.create(cont=request.POST.get('content'))
        return render(request, 'controller/front.html') 
    

def frnt(request):
    data = front.objects.all().order_by()
    context = {
        'content': data        
    }
    return render(request, "controller/front.html", context)
