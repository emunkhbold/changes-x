CREATE TRIGGER t_insert
after insert on django_site
for each row
insert into django_site(domain, name) values('www.test.com', 'test')

DROP TRIGGER IF EXISTS t_insert

SHOW TRIGGERS


CREATE PROCEDURE checkUser(IN name INT, IN uId INT)
select * from auth_user where name=username and uId=id

DROP PROCEDURE checkuser

ALTER PROCEDURE checkUser MODIFIES SQL DATA
SQL SECURITY INVOKER


COMMENT ' ALTER TRIGGER'


ALTER PROCEDURE checkUser SQL SECURITY DEFINER

delimiter //
CREATE PROCEDURE user(IN name varchar(30), IN uId INT)
begin
if isnull(name)
then set name='sublime';
end if;
select * from auth_user where name=username and uId=id;
end;


CREATE PROCEDURE like_user(IN name varchar(30))
SELECT * FROM auth_user WHERE username LIKE CONCAT('%', name, '%')

create procedure site_info(IN sid int , out cls varchar(30))
select ifnull(name, 'NULL'),domain into cls from django_site where id=sid and name 

DELIMITER //
CREATE TRIGGER add_admin
after insert on controller_employers



BEGIN
IF NEW.POSITION=1
insert into auth_user(`id`, `password`,  `is_superuser`, 'username') values(null, hash('sha256', 1, 'news.lname'))
END; //



DELIMITER //
CREATE TRIGGER add_admin
after insert on controller_employers
for each row
BEGIN
IF NEW.POSITION=1
then
insert into auth_user(`id`, `password`,  `is_superuser`, `username`, `is_staff`, `is_active`) values(null, 'pbkdf2_sha256$10000$l2rnIlV90ZA1$KYUMUJse04Fki4fCgM2NGwvCQkhpH6TzmaeR1wNdmRQ=', 1, NEW.lname,1, 1);
end if;
END; //



